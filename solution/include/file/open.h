//Polina Kopylova made it

#include <stdio.h>

#ifndef OPENER
#define OPENER
enum open_status {
    SUCCESS_OPEN,
    ERROR_OPEN
};

static const char *modes_for_open_array[] = {"rb","wb"};

enum read_mode{
    READ=0,
    WRITE=1
};



enum open_status open_file(FILE** file, const char* img_filename, enum read_mode rm);




#endif
