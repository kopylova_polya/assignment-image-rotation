//Polina Kopylova made it

#include "../include/bmp/write.h"
#include "../include/bmp/headers.h"
#include "../include/image/image_struct.h"
#include <stdio.h>
#include <stdlib.h>




enum write_status to_bmp( FILE* output, struct image const* img) {
    int pad = (PIXEL_OFFSET - (int)((PIXEL_STRUCT_SIZE * img->width) % PIXEL_OFFSET)) % PIXEL_OFFSET;

    uint32_t file_size=img->height * img->width * PIXEL_STRUCT_SIZE + img->height * pad + ALL_SIZE;
    uint32_t width=img->width;
    uint32_t height=img->height;
    uint32_t size_image=img->height * img->width;
    struct bmp_header* hd=create_header(file_size,width,height,size_image);
    uint64_t write_status = fwrite(hd, BMP_HEADER_STRUCT_SIZE, 1, output);
    free(hd);

    if (!write_status) return WRITE_ERROR;

    int pxl_count = 0;

    for (size_t n = 0; n < img->height; n++) {
        for (size_t m = 0; m < img->width; m++) {
            write_status = fwrite(img->data + pxl_count, PIXEL_STRUCT_SIZE, 1, output);


            if (!write_status) return WRITE_ERROR;

            pxl_count++;
        }

        fseek(output, (int8_t) pad, SEEK_CUR);
    }
    return WRITE_OK;
}
