#include "../include/bmp/headers.h"
#include "../include/image/image_struct.h"
#include <stdio.h>
#include <stdlib.h>
struct bmp_header* create_header(uint32_t file_size,uint32_t width,uint32_t height,uint32_t size_image){
    struct bmp_header* hd = malloc(BMP_HEADER_STRUCT_SIZE);

    hd->bfType = TYPE_OF_FILES;
    hd->biBitCount = BIT_COUNT;
    hd->biCompression = COMPRESSION;
    hd->bfileSize = file_size;
    hd->biClrImportant = 0;
    hd->bOffBits = ALL_SIZE;
    hd->biSize = INFO_SIZE;
    hd->biXPelsPerMeter = X_PIXELS;
    hd->biYPelsPerMeter = Y_PIXELS;
    hd->biWidth = width;
    hd->biHeight = height;
    hd->biPlanes = PLANES;
    hd->biSizeImage = size_image;
    hd->biClrUsed = 0;
    hd->bfReserved = RESERVED;

    return hd;
}





